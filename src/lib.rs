pub async fn generate_thumbnail(filedata: Vec<u8>, size: Size) -> Result<Vec<u8>, ImageError> {
    let img = Reader::new(Cursor::new(filedata))
        .with_guessed_format()
        .expect("Cursor io to never fail")
        .decode()?;
    let img_size = Size::from(img.dimensions());

    let filter_type = FilterType::Lanczos3;
    let img = if img_size.is_landscape() {
        if size.is_landscape() {
            img.resize(
                size.longest_edge(),
                (size.longest_edge() as f32 * img_size.ratio()) as u32,
                filter_type,
            )
        } else {
            img.resize(
                (size.longest_edge() as f32 * img_size.ratio()) as u32,
                size.longest_edge(),
                filter_type,
            )
        }
    } else if size.is_landscape() {
        img.resize(
            size.longest_edge(),
            (size.longest_edge() as f32 * img_size.ratio()) as u32,
            filter_type,
        )
    } else if size.is_portrait() {
        img.resize(
            (size.longest_edge() as f32 * img_size.ratio()) as u32,
            size.longest_edge(),
            filter_type,
        )
    } else {
        img.resize(
            size.longest_edge(),
            (size.longest_edge() as f32 * img_size.ratio()) as u32,
            filter_type,
        )
    };

    let thumb_size = Size::from(img.dimensions());

    // Crop only centre for now.
    let img = img.crop_imm(
        // TODO allow thumb sizes that are larger than the original
        (thumb_size.width / 2) - (std::cmp::min(thumb_size.width, size.width) / 2),
        (thumb_size.height / 2) - (std::cmp::min(thumb_size.height, size.height) / 2),
        size.width,
        size.height,
    );

    let mut writer: Vec<u8> = Vec::new();
    let encoder = JpegEncoder::new_with_quality(&mut writer, 95);
    img.write_with_encoder(encoder)?;
    Ok(writer)
}

#[derive(Debug)]
pub struct Thumbnail {
    pub url: String,
    pub size: Size,
}

impl Thumbnail {
    pub async fn new(
        media_url: &str,
        storage_path: &str,
        file_name: &str,
        size: Size,
    ) -> Result<Self> {
        let thumbnail_dir = format!("{storage_path}/_thumbs_/{}x{}", size.width, size.height);
        let path = format!("{}/{}", thumbnail_dir, file_name);
        let url = format!(
            "{media_url}/_thumbs_/{}x{}/{}",
            size.width, size.height, file_name
        );
        match tokio::fs::try_exists(&path).await {
            Ok(true) => (),
            Ok(false) | Err(_) => {
                match tokio::fs::try_exists(&thumbnail_dir).await {
                    Ok(true) => (),
                    Ok(false) | Err(_) => {
                        tokio::fs::create_dir_all(&thumbnail_dir).await?;
                    }
                };
                let path = format!("{}/{}", thumbnail_dir, file_name);
                let in_file = tokio::fs::read(format!("{}/{}", storage_path, file_name)).await?;
                let result =
                    generate_thumbnail(in_file, Size::from((size.width, size.height))).await?;

                tokio::fs::write(&path, result).await?;
            }
        };
        Ok(Self { url, size })
    }
}

#[derive(Debug)]
pub struct Size {
    pub width: u32,
    pub height: u32,
}

impl From<(u32, u32)> for Size {
    fn from(value: (u32, u32)) -> Self {
        Self {
            width: value.0,
            height: value.1,
        }
    }
}

impl Size {
    pub fn is_landscape(&self) -> bool {
        self.width > self.height
    }

    pub fn is_portrait(&self) -> bool {
        self.width < self.height
    }

    pub fn longest_edge(&self) -> u32 {
        u32::max(self.width, self.height)
    }

    pub fn shortest_edge(&self) -> u32 {
        u32::min(self.width, self.height)
    }

    pub fn ratio(&self) -> f32 {
        if self.is_landscape() {
            self.width as f32 / self.height as f32
        } else {
            self.height as f32 / self.width as f32
        }
    }
}

use anyhow::Result;
use image::codecs::jpeg::JpegEncoder;
use image::imageops::FilterType;
use image::io::Reader;
use image::GenericImageView;
use image::ImageError;
use std::io::Cursor;

#[cfg(test)]
mod tests {
    use super::*;

    const SIZE: (u32, u32) = (500, 333);

    #[tokio::test]
    async fn landscape_to_landscape() {
        let file = std::fs::read("test_landscape.jpeg").unwrap();
        let result = generate_thumbnail(file, Size::from((SIZE.0, SIZE.1))).await;
        assert!(result.is_ok());
        let _ = std::fs::write(
            format!("test_landscape_{}x{}.jpg", SIZE.0, SIZE.1),
            result.unwrap(),
        );
    }

    #[tokio::test]
    async fn landscape_to_square() {
        let file = std::fs::read("test_landscape.jpeg").unwrap();
        let result = generate_thumbnail(file, Size::from((SIZE.0, SIZE.0))).await;
        assert!(result.is_ok());
        let _ = std::fs::write(
            format!("test_landscape_{}x{}.jpg", SIZE.0, SIZE.0),
            result.unwrap(),
        );
    }

    #[tokio::test]
    async fn landscape_to_portrait() {
        let file = std::fs::read("test_landscape.jpeg").unwrap();
        let result = generate_thumbnail(file, Size::from((SIZE.1, SIZE.0))).await;
        assert!(result.is_ok());
        let _ = std::fs::write(
            format!("test_landscape_{}x{}.jpg", SIZE.1, SIZE.0),
            result.unwrap(),
        );
    }

    #[tokio::test]
    async fn portrait_to_landscape() {
        let file = std::fs::read("test_portrait.jpeg").unwrap();
        let result = generate_thumbnail(file, Size::from((SIZE.0, SIZE.1))).await;
        assert!(result.is_ok());
        let _ = std::fs::write(
            format!("test_portrait_{}x{}.jpg", SIZE.0, SIZE.1),
            result.unwrap(),
        );
    }

    #[tokio::test]
    async fn portrait_to_square() {
        let file = std::fs::read("test_portrait.jpeg").unwrap();
        let result = generate_thumbnail(file, Size::from((SIZE.0, SIZE.0))).await;
        assert!(result.is_ok());
        let _ = std::fs::write(
            format!("test_portrait_{}x{}.jpg", SIZE.0, SIZE.0),
            result.unwrap(),
        );
    }

    #[tokio::test]
    async fn portrait_to_portrait() {
        let file = std::fs::read("test_portrait.jpeg").unwrap();
        let result = generate_thumbnail(file, Size::from((SIZE.1, SIZE.0))).await;
        assert!(result.is_ok());
        let _ = std::fs::write(
            format!("test_portrait_{}x{}.jpg", SIZE.1, SIZE.0),
            result.unwrap(),
        );
    }
}
